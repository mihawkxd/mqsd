pt.bystander = "Inocente"
pt.murderer = "Assassino"
pt.spectator = "Observador"

pt.knife = "Faca"
pt.magnum = "Magnum"
pt.gun = "Arma"
pt.hands = "Mãos"


pt.teamSpectators = "Observadores"
pt.teamPlayers = "Jogadores"
pt.teamAss = "Bolo"

pt.killedTeamKill = "{player} matou um inocente!"
pt.killedMurderer = "{player} matou o assassino"

pt.murdererDeathUnknown = "O assassino morreu de circunstacias misteriosas"

pt.changeTeam = "{player} trocou de time para {team}"
pt.teamMoved = "{player} foi movido para {team}"
pt.teamMovedAFK = "{player} foi movido para {team} por ficar AFK"

pt.spectateFailed = "Você não pode observar. Você não está morto."

pt.murdererDisconnect = "O assassino não aguentou a pressão e saiu"
pt.murdererDisconnectKnown = "O assassino saiu, ele era {murderer}"

pt.winBystanders = "Os inocentes venceram!"
pt.winBystandersMurdererWas = " O assassino era {murderer}"
pt.winMurderer = "O assassino venceu!"
pt.winMurdererMurdererWas = " Ele era {murderer}"

pt.minimumPlayers = "Jogadores insuficientes para inicar a partida."
pt.waitingToStart = "Esperando para começar"
pt.roundStartsInTime = "A rodada começa em {seconds}"
pt.roundStarted = "Uma nova rodada começou"


pt.adminMurdererSelect = "{player} será o assassino no próximo round"
pt.adminMurdererForce = "Forçar assassino próximo round"
pt.adminSpectate = "Observar"
pt.adminMoveToSpectate = "Mover para {spectate}"

pt.mapChange = "Trocando de mapa para {map}"

pt.scoreboard = "Placar"
pt.scoreboardName = "Nome"
pt.scoreboardPing = "Ping"
pt.scoreboardBystanderName = "Pseudónimo"
pt.scoreboardStatus = "Estado"
pt.scoreboardChance = "Chance"
pt.scoreboardRefresh = "Atualizar"

pt.scoreboardJoinTeam = "Entrar"

pt.scoreboardActionAdmin = "Staff"
pt.scoreboardActionMute = "Silenciar"
pt.scoreboardActionUnmute = "Dessilenciar"
pt.scoreboardActionViewProfile = "Ver Perfil"

pt.endroundMurdererQuit = "Os inocentes venceram! O assassino saiu!"
pt.endroundBystandersWin = "Os inocentes venceram!"
pt.endroundMurdererWins = "O assassino venceu!"
pt.endroundMurdererWas = "Assassino:  {murderer}"

pt.endroundLootCollected = "Nome                                              Apelido                       Relíquias"

pt.adminPanel = "Painel de Administrador"

pt.spectating = "Observando"

pt.adMelonbomberWhy = ""
pt.adMelonbomberBy = ""

pt.voiceHelp = "Ajuda"
pt.voiceHelpDescription = "Grite por ajuda"
pt.voiceFunny = "Piada"
pt.voiceFunnyDescription = "Um pouco de humor"
pt.voiceScream = "Gritar"
pt.voiceScreamDescription = "Como uma garotinha"
pt.voiceMorose = "Resmungar"
pt.voiceMoroseDescription = "Sinta a tristeza"


pt.startHelpBystanderTitle = "Você é um inocente"
pt.startHelpBystander = {
	"Porém temos um assassino a solta",
	"Não seja morto"
}


pt.startHelpGunTitle = "Você é um inocente"
pt.startHelpGunSubtitle = "com uma arma secreta! não campere, salve inocentes!"
pt.startHelpGun = {
	"Porém temos um assassino a solta",
	"Salve inocentes e elimine-o"
}

pt.startHelpMurdererTitle = "Você é o assassino"
pt.startHelpMurderer = {
	"Mate todos eles, evite camperar",
	"Não seja pego"
}

pt.murdererFog = "Sua presença maligina está aparecendo!"
pt.murdererFogSub = "Mate alguem para se esconder"

pt.pressEToDisguiseFor1Loot = "[E] Disfarce por 1 relíquia"
pt.pressEToDisguiseFor1Loot2 = "[E] para revistar por 5 relíquias"

pt.playerStatusDead = "Morto"

// ttt_traitor_button compatibility for TTT maps
pt.ttt_tbut_single  = "Uso único"
pt.ttt_tbut_reuse   = "Reutilizável"
pt.ttt_tbut_retime  = "Reutilizável após {num} segundos"
pt.ttt_tbut_waittime  = "Reutilizável em {timesec}"
pt.ttt_tbut_help    = "Pressione {key} para ativar"
